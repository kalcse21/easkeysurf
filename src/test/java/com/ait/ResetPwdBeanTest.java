package com.ait;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



class ResetPwdBeanTest {

	private ResetPwdBean rstpwd;
	
	@BeforeEach
	public void setUp() {
		rstpwd=new ResetPwdBean();
	}
	@Test
	public void setEmailandGetEmailTest() {
		rstpwd.setEmail("xyz.123@gmail.com");
		assertEquals("xyz.123@gmail.com",rstpwd.getEmail());
	}
	@Test
	public void setnewPasswordandGetnewPassowordTest() {
		rstpwd.setNewpassword("ABCD@1234");
		assertEquals("ABCD@1234",rstpwd.getNewpassword());
	}
	@Test
	public void setnewPasswordoneandGetnewPassowordoneTest() {
		rstpwd.setNewpassword1("XYZ@1234");
		assertEquals("XYZ@1234",rstpwd.getNewpassword1());
	}
}
