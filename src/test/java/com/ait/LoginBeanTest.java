package com.ait;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;




class LoginBeanTest {
	
	private LoginBean loginobj;
	
	@BeforeEach
	public void setUp() {
		loginobj=new LoginBean();
	}

	@Test
	public void setEmailandGetEmailTest() {
		loginobj.setEmail("xyz.123@gmail.com");
		assertEquals("xyz.123@gmail.com",loginobj.getEmail());
	}
	@Test
	public void setPasswordandGetPassowordTest() {
		loginobj.setPassword("ABCD@1234");
		assertEquals("ABCD@1234",loginobj.getPassword());
	}

	
}
