package com.ait;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



class UserTest {

	private User user;
	@BeforeEach
	public void setUp() {
		user=new User("MyFirstName", "MyLastName", 1234567890, "Apt no. 1 The Crescent Apartment Dublin 1", false, "xyz@gmail.com", "abcd@1234","novice",0,0);
	}
	@Test
	public void constructTest() {
		assertEquals("MyFirstName",user.getFirstname());
		assertEquals("MyLastName",user.getLastname());
		assertEquals(1234567890,user.getPhonenum());
		assertEquals("Apt no. 1 The Crescent Apartment Dublin 1",user.getAddress());
		assertEquals(false,user.isAdmin());
		assertEquals("xyz@gmail.com",user.getEmail());
		assertEquals("abcd@1234",user.getPassword());
	}
	
	@Test
	public void changeFirstNameTest() {
		user.setFirstname("MyChnagedName");
		assertEquals("MyChnagedName",user.getFirstname());
	}

	@Test
	public void changeLastNameTest() {
		user.setLastname("MyChnagedLastName");
		assertEquals("MyChnagedLastName",user.getLastname());
	}

	@Test
	public void changePhonenumTest(){
		user.setPhonenum(1789456123);
		assertEquals(1789456123,user.getPhonenum());
	}

	@Test
	public void changeAddressTest(){
		user.setAddress("Apt no. 2 The Crescent Apartment Dublin 2");
		assertEquals("Apt no. 2 The Crescent Apartment Dublin 2",user.getAddress());
	}
	
	@Test 
	public void changeAdminTest() {
		user.setAdmin(true);
		assertTrue(user.isAdmin());
	}

	@Test
	public void changeEmailTest() {
		user.setEmail("xyzABC@gmail.com");
		assertEquals("xyzABC@gmail.com",user.getEmail());
	}
	@Test
	public void changePasswordTest() {
		user.setPassword("122Abcd@");
		assertEquals("122Abcd@",user.getPassword());
	}
	@Test
	public void toStringTest() {
		assertEquals(user.toString(),
		"User [firstname=MyFirstName, lastname=MyLastName, phonenum=1234567890, address=Apt no. 1 The Crescent Apartment Dublin 1, admin=false, email=xyz@gmail.com, password=abcd@1234]");
	}
	
	@Test 
	public void constructor2Test() {
		user=new User("newemai1@gmai.com", "newpass@1234");
		assertEquals(user.getEmail(),"newemai1@gmai.com");
		assertEquals(user.getPassword(),"newpass@1234");
	}
}
