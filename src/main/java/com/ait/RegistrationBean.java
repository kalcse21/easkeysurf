package com.ait;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


@ManagedBean
@SessionScoped
public class RegistrationBean implements Serializable{
	private static final long serialVersionUID=1L;
	private String firstname;
    private String lastname;
    private long phonenum;
    private String address;
    private String email;
	private String password;
	private String password1;
	//private User user =new User(firstname, lastname,phonenum, address,email, password);
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public long getPhonenum() {
		return phonenum;
	}
	public void setPhonenum(long phonenum) {
		this.phonenum = phonenum;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword1() {
		return password1;
	}
	public void setPassword1(String password1) {
		this.password1 = password1;
	}
	
	public String createUser() {
		Userdata userdata = Helper.getBean("userdata", Userdata.class);
		
	    if( userdata.emailThereAlready(this.email)<0){
	    	
	    	if (userdata.newuserpwdval(password,password1)) {
	    		User user = new User(this.firstname,this.lastname,this.phonenum,this.address,false,this.email,this.password,null,0.0, 1);
	    		
	    		if (userdata.adduser(user)) {
	    			userdata.setUserasAdmin(email);
	    			return "Login.xhtml";
	    		} else {
	    	    	System.out.println("here");
	    	    	FacesContext context=FacesContext.getCurrentInstance();
	    				FacesMessage errorMessage=
	    						new FacesMessage ("Invalid Username/Password Combination");
	    				errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
	    				context.addMessage(null,  errorMessage);
	    				return (null);
	    	    }
		    }else {
		    	System.out.println("here");
		    	FacesContext context=FacesContext.getCurrentInstance();
					FacesMessage errorMessage=
							new FacesMessage ("Password do not match");
					errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
					context.addMessage(null,  errorMessage);
					return (null);
		    }
		    
	    	
	    }else{
	    	System.out.println("here");
	    
	    	FacesContext context=FacesContext.getCurrentInstance();
				FacesMessage errorMessage=
					new FacesMessage ("The email is already registered");
				errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(null,  errorMessage);
				return (null);
	    }
	    
		
	}

	
	
	
}
