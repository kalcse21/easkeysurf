package com.ait;

import javax.faces.bean.ManagedBean;  
//import javax.faces.context.FacesContext;
//import javax.servlet.http.HttpSession;


 
@ManagedBean  
public class logoutBean { 
	
	boolean userLoggedIn=true;
	boolean adminUserLoggedIn=true;

public String logOut() { 

if(userLoggedIn || adminUserLoggedIn) {
	userLoggedIn=false;
	adminUserLoggedIn=false;
}
return "/main.xhtml?faces-redirect=true";

}


} 
 
