package com.ait;

import java.util.ArrayList;

public class User {
	private String firstname;
    private String lastname;
    private long phonenum;
    private String address;
    private boolean admin;
    private String email;
	private String password;
	boolean canEdit; 
	private String packages;
	private double bill;
	private int quantity;
	private double total;
	
	
	public User(String firstname, String lastname, long phonenum, String address, boolean admin,String email,
			String password,String packages,double bill,int quantity) {
		
		this.firstname = firstname;
		this.lastname = lastname;
		this.phonenum = phonenum;
		this.address = address;
		this.admin = admin;
		this.email = email;
		this.password = password;
		this.packages=packages;
		this.bill=bill;
		this.quantity=quantity;
		
	}
	
	public User(String email, String password) {
		
		this.email = email;
		this.password = password;
	}

	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public long getPhonenum() {
		return phonenum;
	}
	public void setPhonenum(long phonenum) {
		this.phonenum = phonenum;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isCanEdit() {
		return canEdit;
	}

	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	
	public String getPackages() {
		return packages;
	}

	public void setPackages(String packages) {
		this.packages = packages;
	}

	public double getBill() {
		return bill;
	}

	public void setBill(double bill) {
		this.bill = bill;
	}

	
	
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total =this.bill*this.quantity;
	}

	@Override
	public String toString() {
		return "User [firstname=" + firstname + ", lastname=" + lastname + ", phonenum=" + phonenum + ", address="
				+ address + ", admin=" + admin + ", email=" + email + ", password=" + password + "]";
	}
	
}
