package com.ait;

import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


@ManagedBean
@SessionScoped
public class Userdata {
	private static ArrayList<User> users=new ArrayList<User>();
	private boolean loggedin;
	public ArrayList<User> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}

	@PostConstruct
	public void init() {
		System.out.println("POST Construct");
		User firstUser = new User("kalyani", "palada", 0, "Oranmore-Galway",true,"kal.pal@gmail.com",
			"password",null,0,1);
		users.add(firstUser);
		User secondUser = new User("ka", "pa", 0, "Galway",false, "kal1.pal@gmail.com",
				"secret","Novice",0.0,1);
		users.add(secondUser);
	}
	
	public boolean existUser(String email) {
		boolean existuser = false;
		for (User user:users) {
			System.out.println("USER is : "+user);
			System.out.println("USER is : "+user.getEmail());
			if((user.getEmail().equals(email))){
				existuser=true;
				break;
			}
		}
		return existuser;
	}
	
	public int emailThereAlready(String email) {
		int index = -1;
		for (int i = 0; i < users.size(); i++) {
			User user = users.get(i);
			if (user.getEmail().equals(email)) {
				index = i;
				break;
			}
		}
		return index;
	}
	
	
	public boolean validateUser(String email, String password) {
		
		for (User user:users) {
			if((user.getEmail().equals(email))&&(user.getPassword().equals(password) )){
			
				this.loggedin=true;
				break;
			}
		}
		return loggedin;
	}
	
	public boolean pwdnotnull(String newpassword, String newpassword1) {
		boolean pwdnull=false;
		if (newpassword.isEmpty()||newpassword1.isEmpty()){
			pwdnull=true;
		}
		return pwdnull;
	}
	
	public boolean newuserpwdval(String password, String password1) {
//		boolean pwdnull=true;
////		if ((password.isEmpty()||password1.isEmpty())&&(password.equals(password1) )){
////			pwdnull=true;
////		}
//		return pwdnull;
		boolean registerUser=false;
		for (User user:users) {
			if(password.equals(password1)){
				registerUser=true;
				break;
			}
			//System.out.println(password);
		}
		return registerUser;
	}
	
	public boolean adduser(User user) {
		
		int size1=0,size2=0;
		boolean suceess=false;
		size1=users.size();
		users.add(user);
		size2=users.size();
		if (size2==size1+1) {
			suceess= true;
		}else {
			suceess=false;
		}
		return suceess;
	}
	
	
	public boolean resetpassword(String email, String newpassword, String newpassword1) {
		boolean change=false;
		for (User user:users) {
			if((user.getEmail().equals(email))&&(newpassword.equals(newpassword1) )){
				user.setPassword(newpassword);
				change =true;
				break;
			}
		}
		return change;
	}
	public boolean isUserAdmin(String email) {
		boolean uadmin=false;
		for (int i = 0; i < users.size(); i++) {
			User user = users.get(i);
			if (user.getEmail().equalsIgnoreCase(email)) {
				uadmin =user.isAdmin();
				break;
			}
		}
		return uadmin;
	}


	public void setUserasAdmin(String email) {
		for (int i = 0; i < users.size(); i++) {
			User user = users.get(i);
			if (user.getEmail().contains("@easkeysurf.com")) {
				user.setAdmin(true);
				break;
			}
			
		}
		
		
	}
	

	
	public static void display(String email) {
		for (User user : users)
			System.out.println(user);

	}
	
//	public boolean validateUserRegpass(String password, String password1) {
//		boolean registerUser=false;
//		for (User user:users) {
//			if(password.equals(password1)){
//				registerUser=true;
//				break;
//			}
//			//System.out.println(password);
//		}
//		return registerUser;
//	}
	
	public String editUser(User user) {
		user.setCanEdit(true);
		return null;
	}
	
	public String saveAction() {
        for(User user:users) {
            user.setCanEdit(false);
        }
        return null;
    }

}
