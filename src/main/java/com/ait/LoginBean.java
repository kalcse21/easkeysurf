package com.ait;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


@ManagedBean
@SessionScoped
public class LoginBean implements Serializable{
	private static final long serialVersionUID=1L;
	
	String email, password;
		
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String validateUserLogin() {
		Userdata userdata = Helper.getBean("userdata", Userdata.class);
		System.out.println("EMAIL is :"+email);
	    if (!userdata.existUser(email)) {
	    	System.out.println("here");
	    	FacesContext context=FacesContext.getCurrentInstance();
				FacesMessage errorMessage=
						new FacesMessage ("The email is not registered");
				errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(null,  errorMessage);
				return (null);
	    }
	    
		
		if (userdata.validateUser(email, password)) {
			if(userdata.isUserAdmin(email)) {
				return "/Adminhome.xhtml?faces-redirect=true";
			}else {
				return "/home.xhtml?faces-redirect=true";
			}
		} else {
	    	System.out.println("here");
	    	FacesContext context=FacesContext.getCurrentInstance();
				FacesMessage errorMessage=
						new FacesMessage ("Invalid Username/Password Combination");
				errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(null,  errorMessage);
				return (null);
	    }
	}

}
