package com.ait;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


@ManagedBean
@SessionScoped
public class ResetPwdBean implements Serializable{
	private static final long serialVersionUID=1L;
	private String email;
	
	private String newpassword;
	private String newpassword1;
	
	
	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getNewpassword() {
		return newpassword;
	}


	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}


	public String getNewpassword1() {
		return newpassword1;
	}


	public void setNewpassword1(String newpassword1) {
		this.newpassword1 = newpassword1;
	}


	public String passwordChange() {
		Userdata userdata = Helper.getBean("userdata", Userdata.class);
		if (!userdata.existUser(email)) {
	    	System.out.println("here");
	    	FacesContext context=FacesContext.getCurrentInstance();
				FacesMessage errorMessage=
						new FacesMessage ("The email is not registered");
				errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(null,  errorMessage);
				return (null);
	    }
		if (userdata.pwdnotnull(newpassword,newpassword1)) {
			System.out.println("here");
	    	FacesContext context=FacesContext.getCurrentInstance();
				FacesMessage errorMessage=
						new FacesMessage ("The password field cannot be empty");
				errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(null,  errorMessage);
				return (null);
		}
		
		if (userdata.resetpassword(email,newpassword,newpassword1)) {
			return "Login.xhtml";
			
	    } else {
	    	System.out.println("here");
	    	FacesContext context=FacesContext.getCurrentInstance();
				FacesMessage errorMessage=
						new FacesMessage ("The Passwords do not match");
				errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(null,  errorMessage);
				return (null);
	    }
	}

	
}
